from sklearn.tree import DecisionTreeClassifier


class DecisionTreeModule:
    """
    Make use of Decison Trees
    """

    def __init__(self, train_set):
        self.train_set = train_set
        # self.clf = svm.OneClassSVM()
        self.clf = DecisionTreeClassifier()

    def train(self):
        X = list()
        y = list()
        print("DecisionTreeModule: Generating points")
        for row in self.train_set:
            X.append(row["vector"])
            y.append(row["value"])
        print("DecisionTreeModule: Fitting...")
        self.clf.fit(X, y)
        print("DecisionTreeModule: End.")

    def test(self, url):
        answer = self.clf.predict([url['vector']])
        if answer > 0.5:
            return 1
        else:
            return 0
