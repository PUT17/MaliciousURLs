from random import getrandbits


class DummySystem:
    """
    Stupidest of the stupidest algorithm. Return random values.
    """

    def __init__(self, train_set):
        self.train_set = train_set

    def train(self):
        for row in self.train_set:
            # row["vector"]
            #   - contains single URL vector
            # row["value"]
            #   - contains value of URL (1 if malware, 0 if benign)
            pass
        pass

    def test(self, url):
        # return 1 or 0 (no 0.765, nor 0.543 nor 0.123 etc.)
        return getrandbits(1)
