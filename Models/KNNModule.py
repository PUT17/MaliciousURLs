from sklearn.neighbors import KNeighborsClassifier


class KNNModule:
    """
    Make use of KNN
    """

    def __init__(self, train_set, n_neighbors=5):
        self.train_set = train_set
        # self.clf = svm.OneClassSVM()
        self.clf = KNeighborsClassifier(n_neighbors=n_neighbors)

    def train(self):
        X = list()
        y = list()
        print("KNNModule: Generating points")
        for row in self.train_set:
            X.append(row["vector"])
            y.append(row["value"])
        print("KNNModule: Fitting...")
        self.clf.fit(X, y)
        print("KNNModule: End.")

    def test(self, url):
        answer = self.clf.predict([url['vector']])
        if answer > 0.5:
            return 1
        else:
            return 0
