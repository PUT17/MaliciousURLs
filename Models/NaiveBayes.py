# Import Naive Bayes
from sklearn.naive_bayes import BernoulliNB


class NaiveBayesModule:
    def __init__(self, train_set):
        self.train_set = train_set
        self.model = BernoulliNB()

    def train(self):
        X = list()
        Y = list()
        for row in self.train_set:
            X.append(row["vector"])
            Y.append(row["value"])
        print("NaiveBayesModule: Fitting...")
        self.model.fit(X, Y)
        print("NaiveBayesModule: End.")

    def test(self, url):
        BernoulliNB(alpha=0.5, binarize=0.9, class_prior=None, fit_prior=True)
        answer = self.model.predict([url['vector']])
        if answer > 0.5:
            return 1
        else:
            return 0
