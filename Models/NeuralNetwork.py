from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SigmoidLayer


class NeuralNetwork:
    def __init__(self, train_set, input_layers=58, hidden_layers=5):
        """
        Create lib object.
        :param train_set:
        :param input_layers:
        :param hidden_layers:
        """
        self.train_set = train_set
        self.input_layers = input_layers
        self.hidden_layers = hidden_layers
        self.neural_net = 0

    def train(self):
        """
        Train own neural network
        :return neural_net:
        """
        # Using basic neural network from pybrain
        # input: 58 (vector length), output: 1
        # hidden layer: 3 (experimentation recommended!)
        if not self.neural_net:
            self.neural_net = buildNetwork(self.input_layers, self.hidden_layers, 1, outclass=SigmoidLayer)
        superv_train_set = SupervisedDataSet(self.input_layers, 1)
        # Create Data set for neural network training
        # # (based on first [0-endIndex] URLs)
        for i in range(len(self.train_set)):
            superv_train_set.addSample(self.train_set[i]["vector"], self.train_set[i]["value"])

        # Train network... few times or train until convergence
        trainer = BackpropTrainer(self.neural_net, superv_train_set)
        # trainer.trainUntilConvergence()
        print("NeuralNetModule: Learning process started...")
        mismtach = 1
        epochs = 0
        while mismtach > 0.09 and epochs < 10:
            epochs += 1
            mismtach = trainer.train()
            print("NeuralNetModule: mismatch =", mismtach)
        print("NeuralNetModule: End.")

        # self.neural_net = neural_net
        return self.neural_net

    def test(self, url):
        """
        Test some single url.
        :param url:
        :return:
        """
        if self.neural_net.activate(url['vector']) > 0.7:
            return 1
        else:
            return 0
