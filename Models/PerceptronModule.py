from sklearn.linear_model import Perceptron


class PerceptronModule:
    """
    Scikitlearn Linear Perceptron
    """

    def __init__(self, train_set):
        self.train_set = train_set
        self.clf = Perceptron(n_jobs=-1)  # -1 to use all CPUs

    def train(self):
        X = list()
        y = list()
        print("PerceptronModule: Generating points")
        for row in self.train_set:
            X.append(row["vector"])
            y.append(row["value"])
        print("PerceptronModule: Fitting...")
        self.clf.fit(X, y)
        print("PerceptronModule: End.")

    def test(self, url):
        answer = self.clf.predict([url['vector']])
        if answer > 0.5:
            return 1
        else:
            return 0
