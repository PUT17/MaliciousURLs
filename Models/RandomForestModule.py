from sklearn.ensemble import RandomForestClassifier


class RandomForestModule:
    """
    Make use of Decison Trees
    """

    def __init__(self, train_set):
        self.train_set = train_set
        # self.clf = svm.OneClassSVM()
        self.clf = RandomForestClassifier()

    def train(self):
        X = list()
        y = list()
        print("RandomForestModule: Generating points")
        for row in self.train_set:
            X.append(row["vector"])
            y.append(row["value"])
        print("RandomForestModule: Fitting...")
        self.clf.fit(X, y)
        print("RandomForestModule: End.")

    def test(self, url):
        answer = self.clf.predict([url['vector']])
        if answer > 0.5:
            return 1
        else:
            return 0
