from sklearn import svm


class SVMModule:
    """
    Make use of Support Vector Machine
    """

    def __init__(self, train_set):
        self.train_set = train_set
        # self.clf = svm.OneClassSVM()
        self.clf = svm.SVC()  # clf, X, y names used for keep tutorial name convention

    def train(self):
        X = list()
        y = list()
        print("SVMModule: Generating points")
        for row in self.train_set:
            X.append(row["vector"])
            y.append(row["value"])
        print("SVMModule: Fitting...")
        self.clf.fit(X, y)
        print("SVMModule: End.")

    def test(self, url):
        answer = self.clf.predict([url['vector']])
        if answer > 0.5:
            return 1
        else:
            return 0
