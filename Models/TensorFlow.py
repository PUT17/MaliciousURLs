import tensorflow as tf
import numpy as np


class TensorFlow:
    # wont work
    def __init__(self, train_set):
        self.estimator = None
        self.train_set = train_set

    def train(self):
        X = list()
        y = list()
        for row in self.train_set:
            X.append(row["vector"])
            y.append(row["value"])
        x_train = np.asarray(X, dtype=np.float32)
        y_train = np.asarray(y, dtype=np.float32)
        feats = tf.contrib.learn.infer_real_valued_columns_from_input(x_train)
        # feats = tf.feature_column.numeric_column("x")
        self.estimator = tf.estimator.DNNClassifier(feature_columns=feats, hidden_units=[58, 58, 58])
        input_fn_train = tf.estimator.inputs.numpy_input_fn(x={"x": x_train}, y=y_train, shuffle=False)
        self.estimator.train(input_fn=input_fn_train)

    def test(self, url):
        x_test = np.asarray(url['vector'], dtype=np.float32)
        y_test = np.asarray(url['value'], dtype=np.float32)
        input_fn_predict = tf.estimator.inputs.numpy_input_fn(x={"x": x_test}, y=y_test, shuffle=False)
        answer = self.estimator.predict(input_fn=input_fn_predict)
        print("bu")
        if answer > 0.5:
            return 1
        else:
            return 0

