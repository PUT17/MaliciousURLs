# Table of Contents
1. [MaliciousURLs](#maliciousurls)
2. [How to use](#how-to-use)
3. [Architectures](#architectures)

# MaliciousURLs
Simple version of application to detecting malicious url.

Data set records has in each row:
- url
- comma
- bad/good 

Used libraries:
- sklearn used for obtain classification, preparing data and cleaning data, e. stamping, lemmatization, stop words, 
- pandas used for store data, 
- numpy as above
- tensorflow predictor
- matplotlib

# How to use

1. Obtain repository with ```git clone https://gitlab.com/PUT17/MaliciousURLs.git```
2. (optional) Run pip install -c .\requirements.txt to install libraries
3. Run ```python main.py```

# Architectures

- folder ```Models``` contains model of estimators, each model have functions ```test(self)``` and ```train(self)```
- folders ```*xlsx``` are not important, now stored final excel files with statistics (after executed program this result wil be in ```output``` folder)
- folder ```lib``` contains main classes:
 - ```preprocess``` class resposible for mining features of URLs
 - ```OtherFunctions.py``` set of few functions e.g. (to create data set, to divide data etc. )
 - ```CalculationResult``` class specially written to keep results
 - ```MaliciuosURLs``` main class, here everything is using, calculating, ,fitting, predicting, creating result file etc.
- ```maint.py``` simple example of uses 
- folder ```source``` contains two files with data to train and test


All rights reserved.