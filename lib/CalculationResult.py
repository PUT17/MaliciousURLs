class CalculationResult:
    """
    Class to represent result of calculation.
    """

    def __init__(self, name, precision=0, recall=0, true_positives=0, false_positives=0, false_negatives=0,
                 true_negatives=0, test_time=0, id=0):
        """
        Create and init object.
        :param name:
        :param precision:
        :param recall:
        :param true_positives:
        :param false_positives:
        :param false_negatives:
        :param true_negatives:
        :param test_time:
        :param id:
        """
        self.true_negatives = true_negatives
        self.name = name
        self.precision = precision
        self.recall = recall
        self.true_positives = true_positives
        self.false_positives = false_positives
        self.false_negatives = false_negatives
        self.id = id
        self.test_time = test_time

    def calculate(self):
        """
        Calculate precision and recall.
        :return:
        """
        try:
            self.precision = self.true_positives / (self.true_positives + self.false_positives)
            self.recall = self.true_positives / (self.true_positives + self.false_negatives)
        except ZeroDivisionError:
            print("You can't divide by zero, you're silly.")

    def view(self):
        """
        Print all fields.
        :return:
        """
        print("Precision:\t\t\t", self.precision)
        print("Recall:\t\t\t\t", self.recall)
        print("True positives:\t\t", self.true_positives)
        print("False positives:\t", self.false_positives)
        print("False negatives:\t", self.false_negatives)
        print('Test time:\t\t\t\t', self.test_time)
        print("False negatives:\t", self.false_negatives)
        print("True_negatives:\t", self.true_negatives)

    def get_parameters_vector(self):
        """
        Return list of lib fields (name, precision, recall, true positives, false positives, false negatives, test time, id).
        :return:
        """
        return [self.name, self.precision, self.recall, self.true_positives, self.false_positives, self.false_negatives,
                self.true_negatives, self.test_time, self.id]
