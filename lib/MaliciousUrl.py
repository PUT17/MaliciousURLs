import operator
import pandas as pd
import os

from lib.CalculationResult import CalculationResult
from timeit import default_timer as timer
from lib.OtherFunctions import GetUrlVector
import matplotlib.pyplot as plt


class MaliciousUrl:
    def __init__(self, use_test_set_to_learn=0, name='FinalAlgorithm'):
        self.last_statistics = 0
        self.use_test_set_to_learn = use_test_set_to_learn
        self.modelsList = list()
        self.models_weight = list()
        self.name = name
        self.final_statistics = 0
        self.last_statistics_tmp = 0
        self.tested_url = 0
        self.final_statistics_after_automatic_weight = 0

    def check_statistics(self, test_set):
        """
        View statistic (recall precision) for each model.
        :param test_set:
        """
        print("Calculating statistics...")
        total_result = list()
        for model in self.modelsList:
            print(" for model: ", str(type(model['model']).__name__))
            model_result = CalculationResult(str(type(model['model']).__name__), id=model['id'])
            start = timer()
            for i in range(len(test_set)):
                answer = model['model'].test(test_set[i])
                value = test_set[i]["value"]
                if answer:  # malware link detected (1)
                    if value == 1:
                        model_result.true_positives += 1
                    else:
                        model_result.false_positives += 1
                else:
                    if value == 1:
                        model_result.false_negatives += 1
                    else:
                        model_result.true_negatives += 1
            end = timer()

            model_result.calculate()
            model_result.test_time = end - start
            total_result.append(model_result)
            if self.use_test_set_to_learn:
                model['model'].train_set = test_set
                model['model'].train()

        algorithm_list = list()
        total_data = list()
        parametr_list = ['precision', 'recall', 'true_positives', 'false_positives', 'false_negatives',
                         'true_negatives',
                         'test_time', 'id']
        for result in total_result:
            algorithm_list.append(result.name)
            total_data.append(result.get_parameters_vector()[1:])

        df = pd.DataFrame(total_data, algorithm_list, parametr_list)
        self.last_statistics = df.merge(self.last_statistics, on='id', how='left', left_index=True)
        self.tested_url += len(test_set)
        
    def check_statistics_with_cross_validation(self, train_sets, test_sets):
        """
        View statistic (recall precision) for each model, use cross_validating
        :param test_sets:
        :param train_sets:
        """
        print("Calculating statistics...")
        total_result = list()
        for model in self.modelsList:
            test_time_start = 0
            test_time = 0
            train_time_start = 0
            train_time = 0
            models_result = list()
            for x in range(len(test_sets)):
                print(" for model: ", str(type(model['model']).__name__))
                print('\tTrain %d subset start..' % (x,))
                train_time_start = timer()
                model['model'].train_set = train_sets[x]
                model['model'].train()
                train_time_end = timer()
                train_time += train_time_end - train_time_start
                print('\tTrain %d subset end..' % (x,))
                model_result = CalculationResult(str(type(model['model']).__name__), id=model['id'])
                print('\tTest %d subset start..' % (x,))
                test_time_start = timer()
                for i in range(len(test_sets[x])):
                    answer = model['model'].test(test_sets[x][i])
                    value = test_sets[x][i]["value"]
                    if answer:  # malware link detected (1)
                        if value == 1:
                            model_result.true_positives += 1
                        else:
                            model_result.false_positives += 1
                    else:
                        if value == 1:
                            model_result.false_negatives += 1
                        else:
                            model_result.true_negatives += 1
                test_time_end = timer()

                model_result.calculate()
                model_result.test_time += test_time_end - test_time_start
                if self.use_test_set_to_learn:
                    model['model'].train_set = test_sets[x]
                    model['model'].train()
                models_result.append(model_result)
                print('\tTest %d subset end..' % (x,))

            model_result = CalculationResult(str(type(model['model']).__name__), id=model['id'])
            model_result.precision = sum([model.precision for model in models_result]) / len(models_result)
            model_result.recall = sum([model.recall for model in models_result]) / len(models_result)
            model_result.true_negatives = sum([model.true_negatives for model in models_result]) / len(models_result)
            model_result.name = models_result[0].name
            model_result.id = models_result[0].id
            model_result.true_positives = sum([model.true_positives for model in models_result]) / len(models_result)
            model_result.false_negatives = sum([model.false_negatives for model in models_result]) / len(models_result)
            model_result.false_positives = sum([model.false_positives for model in models_result]) / len(models_result)
            model_result.test_time = sum([model.test_time for model in models_result]) / len(models_result)
            total_result.append(model_result)

        algorithm_list = list()
        total_data = list()
        parametr_list = ['precision', 'recall', 'true_positives', 'false_positives', 'false_negatives',
                         'true_negatives',
                         'test_time', 'id']
        for result in total_result:
            algorithm_list.append(result.name)
            total_data.append(result.get_parameters_vector()[1:])

        df = pd.DataFrame(total_data, algorithm_list, parametr_list)
        self.last_statistics = df.merge(self.last_statistics, on='id', how='left', left_index=True)
        self.tested_url += len(test_sets[0])

    def reset_statitisc(self):
        """
        Reset calculated statistics
        """
        self.last_statistics = self.last_statistics_tmp

    def train_all_models(self):
        """
        Train all our estimators
        """
        algorithm_list = list()
        total_data = list()
        parameters = ['train_time', 'id']

        for model in self.modelsList:
            start = timer()
            model['model'].train()
            end = timer()
            algorithm_list.append(str(type(model['model']).__name__))
            total_data.append([end - start, model['id']])
        self.last_statistics = pd.DataFrame(total_data, algorithm_list, parameters)
        self.last_statistics_tmp = pd.DataFrame(total_data, algorithm_list, parameters)

    def view_statistics(self):
        """
        View statistics on console
        """
        print("\033[95m\n\nSTATISTICS: \033[0m")
        print("\nTested URLs: ", self.tested_url, "\n")
        with pd.option_context('display.width', 1000, 'display.max_columns', 1000):
            column_names = self.last_statistics.columns.tolist()
            column_names = column_names[:-2] + column_names[-1:] + column_names[-2:-1]
            print(self.last_statistics[column_names])

    def get_charts(self, view_on_screen=0, destination='output/'):
        ax = self.last_statistics[['precision', 'recall']].plot(kind='bar', title="Chart", figsize=(15, 10),
                                                                legend=True, fontsize=12)
        ax.set_xlabel("Precision", fontsize=12)
        ax.set_ylabel("Recall", fontsize=12)
        fig = ax.get_figure()
        fig.savefig(destination + 'Precision_Recall.png')  # save the figure to file
        if view_on_screen:
            plt.show()
        plt.close(ax)

    def statistics_to_csv(self, file_name):
        """
        Put last statistics to csv file.
        :param file_name:
        :return:
        """
        try:
            if not os.path.exists('output'):
                os.makedirs('output')
            column_names = self.last_statistics.columns.tolist()
            column_names = column_names[:-2] + column_names[-1:] + column_names[-2:-1]
            self.last_statistics[column_names].to_csv('output/' + file_name, sep=',', encoding='utf-8')
        except Exception:
            print('No recent statistics are available.')

    def statistics_to_excel(self, file_name):
        """
        Put last statistics to excel file.
        :param file_name:
        :return:
        """
        try:
            if not os.path.exists('output'):
                os.makedirs('output')
            column_names = self.last_statistics.columns.tolist()
            column_names = column_names[:-2] + column_names[-1:] + column_names[-2:-1]
            writer = pd.ExcelWriter('output/' + '%s.xlsx' % file_name, engine='xlsxwriter')
            self.last_statistics[column_names].to_excel(writer, sheet_name='Sheet1')
            self.final_statistics.to_excel(writer, startrow=len(self.last_statistics) + 3)
            if not self.final_statistics_after_automatic_weight.empty:
                self.final_statistics_after_automatic_weight.to_excel(writer, startrow=len(self.last_statistics) +
                                                                                       len(self.final_statistics) + 6)
            writer.save()
        except Exception as e:
            print('No recent statistics are available' + e.message)

    def add_model(self, model, weight=1):
        """
        Add model to application, must have field train_set, metod train and test
        :param model:
        """
        self.modelsList.append({'model': model, 'id': id(model)})
        self.models_weight.append({'id': id(model), 'name': str(type(model).__name__), 'weight': weight})
    
    def calculate_weight_of_algorithm(self, dict_of_weight=None, precision=0.01):
        """
        Method to automate recalculate weight of algorithm.
        :param precision:
        :param dict_of_weight: Dict includes weight each feature of algorithm. {'precision': 0.2, 'recall': 0.3, 'true_positives': .1,
        'false_positives': 0.4, 'false_negatives':0.1, 'true_negatives': 1,'test_time':.8, 'train_time':0.4}
        :return:
        """
        print('\nAutomatic calculation weights of algorithms started...')
        if dict_of_weight is None:
            dict_of_weight = {'precision': 10000, 'recall': 10000, 'true_positives': 1, 'false_positives': 1,
                              'false_negatives': 1, 'true_negatives': 1, 'test_time': 100, 'train_time': 100}

        total_points = dict()
        maxx = max([max(d.values()) for d in self.last_statistics.drop('id', axis=1).to_dict(orient='records')])
        minn = 0
        for row in self.last_statistics.to_dict(orient='records'):
            points = 0
            for k, val in row.items():
                if k == 'id':
                    continue
                if k not in dict_of_weight:
                    if k != 'precision' and k != 'recall' and k != 'true_positives' and k != 'true_negatives':
                        t = ((row[k]) - minn) / (maxx - minn)
                        points -= t * 1
                    else:
                        points += ((row[k] - minn) / (maxx - minn)) * 1
            for key, value in dict_of_weight.items():
                try:
                    if key != 'precision' and key != 'recall' and key != 'true_positives' and key != 'true_negatives':
                        points -= (((row[key]) - minn) / (maxx - minn)) * value
                    else:
                        points += ((row[key] - minn) / (maxx - minn)) * value
                except Exception:
                    print('No features in statistics')
            total_points[row['id']] = points
        sorted_x = sorted(total_points.items(), reverse=True, key=operator.itemgetter(1))
        w = 1
        point = sorted_x[0][1]
        print('\nNew weight:')
        for i in range(len(sorted_x)):
            if i != 0:
                if abs(sorted_x[i][1] - point) > precision:
                    w -= 1 / len(self.modelsList)
                else:
                    point = sorted_x[i][1]
            id = int(sorted_x[i][0])
            modl = [model for model in self.models_weight if model['id'] == id][0]
            modl['weight'] *= w
            print(str(modl['name']) + ': ' + str(modl['weight']))
        print('Automatic calculation of weights: End.')

    def check_final_statistics(self, url_set, weighted_algorithms=0, after_automatic_weight=0):
        """
        View final statistic (recall precision).
        :param url_set:
        :param weighted_algorithms:
        """
        if not isinstance(url_set[0], dict):
            set_tmp = list()
            for url in url_set:
                set_tmp.append({
                    "url": url,
                    "value": 0,
                    "vector": GetUrlVector(url)})
            url_set = set_tmp

        if not weighted_algorithms:
            for url in url_set:
                answers = list()
                for model in self.modelsList:
                    answers.append(model['model'].test(url))
                if answers.count(1) >= (len(self.modelsList)):
                    url['value'] = 1
        else:
            model_result = CalculationResult(self.name, id=0)
            start = timer()
            for i in range(len(url_set)):
                answers = list()
                for model in self.modelsList:
                    weight = [d for d in self.models_weight if d['id'] == model['id']][0]['weight']
                    answers.append(model['model'].test(url_set[i]) * weight)
                note = round(sum(answers) / sum([w['weight'] for w in self.models_weight]))
                value = url_set[i]['value']

                if note:  # malware link detected (1)
                    if value == 1:
                        model_result.true_positives += 1
                    else:
                        model_result.false_positives += 1
                else:
                    if value == 1:
                        model_result.false_negatives += 1
                    else:
                        model_result.true_negatives += 1
            end = timer()
            model_result.calculate()
            model_result.test_time = end - start
            parameters_list = ['precision', 'recall', 'true positives', 'false positives', 'false negatives',
                               'true_negatives', 'test_time', 'id']
            if after_automatic_weight:
                self.final_statistics_after_automatic_weight = pd.DataFrame([model_result.get_parameters_vector()[1:]],
                                                                            [self.name], parameters_list)
            else:
                self.final_statistics = pd.DataFrame([model_result.get_parameters_vector()[1:]], [self.name],
                                                     parameters_list)

            print('\n')
            with pd.option_context('display.width', 1000, 'display.max_columns', 1000):
                print(self.final_statistics[parameters_list[:-1]])

            if after_automatic_weight:
                print('\n')
                with pd.option_context('display.width', 1000, 'display.max_columns', 1000):
                    print(self.final_statistics_after_automatic_weight[parameters_list[:-1]])
        return url_set
