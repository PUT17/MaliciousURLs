import csv
import os.path
import pickle
from random import shuffle
from sys import stdout
from lib.Preprocess import Preprocess
from sklearn.model_selection import KFold, StratifiedKFold

def GetUrlVector(url):
    """
    Get 58-elements vector for selected url (based on Preprocess file)
    :param url:
    :return:
    """
    data = Preprocess(url)
    data.separateIntoThree()
    lenFeature = data.getLengthFeatures()
    countFeature = data.getCountFeatures()
    ratioFeature = data.getRatioFeatures()
    binaryFeature = data.getBinaryFeatures()
    wholeSingleFeature = lenFeature + countFeature + ratioFeature + binaryFeature
    return wholeSingleFeature


def createDataset(file_name):
    """
     Create Dataset: list of JSON object. Each object
     contains data about single URL: url, value (bad or good)
     and calculated vector of features.
    :return dataset:
    """

    dataset = list()
    if os.path.exists("output/" + file_name + ".pickle"):
        print("Loading existing dataset from file")
        dataset = pickle.load(open("output/" + file_name + ".pickle", "rb"))
    else:
        print("Creating dataset file")
        with open("source/" + file_name, encoding='utf-8', errors='ignore') as csvfile:
            reader = csv.DictReader(csvfile)
            i = 1
            for row in reader:
                if row['y'] == "bad":
                    val = 1
                else:
                    val = 0
                dataset.append({
                    "url": row['url'],
                    "value": val,
                    "vector": GetUrlVector(row['url'])
                })
                stdout.write("\r%d links added. Current: %s" % (i, str(row['url']).encode("utf-8")[:60]) )
                stdout.flush()
                i += 1
        if not os.path.exists('output'):
            os.makedirs('output')
        pickle.dump(dataset, open("output/" + file_name + ".pickle", "wb"))
    # Dataset should be shuffled. All bad links are located and the
    # beggining of csv file
    shuffle(dataset)
    print("Dataset shuffled.")
    return dataset


def divide_data_set_kfold(data_set, n_splits=2, shuffle=False):
    """
    Divide data set to train and test sets using kfold.
    :param shuffle:
    :param n_splits:
    :param data_set:
    :return:
    """

    kf = KFold(n_splits=n_splits, shuffle=shuffle)

    f_train = list()
    f_test = list()
    # kf.split(data_set,)
    for tr, te in kf.split(data_set,):
        test_set = list()
        train_set = list()
        for t in tr:
            train_set.append(data_set[t])
        for t in te:
            test_set.append(data_set[t])
        f_train.append(train_set)
        f_test.append(test_set)

    return f_train, f_test


def divide_data_set(data_set, train_percent):
    """
    Divide data_set to train_set and test_set
    :param data_set:
    :param train_percent:
    :return: train_set, test_set
    """
    train_set = data_set[:round(len(data_set) * train_percent)]
    test_set = data_set[round(len(data_set) * train_percent):]
    return train_set, test_set
