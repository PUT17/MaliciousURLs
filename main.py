from Models.DecisionTree import DecisionTreeModule
from lib.MaliciousUrl import MaliciousUrl
from lib.OtherFunctions import createDataset, divide_data_set, divide_data_set_kfold

from sklearn.model_selection import KFold

from Models.NeuralNetwork import NeuralNetwork
from Models.DummySystem import DummySystem
from Models.PerceptronModule import PerceptronModule
from Models.SVMModule import SVMModule
from Models.NaiveBayes import NaiveBayesModule
from Models.KNNModule import KNNModule
from Models.RandomForestModule import RandomForestModule

from numpy.random import seed as NPseed
from random import seed as Rseed

Rseed(5)  # used in dataset shuffle
NPseed(5)  # pybrain use numpy.random to set values of neural network weights
print("\033[93m \nWarning: Working with fixed random seed!\n \033[0m")

if __name__ == "__main__":
    data_set = createDataset('final1.csv')
    train_set, test_set = divide_data_set(data_set, .1)
    train_sets, test_sets = divide_data_set_kfold(data_set, n_splits=4, shuffle=False)

    # print(len(train_sets[1]))
    # for tr in range(5):
    #     print(train_sets[1][tr])
    # print()
    #
    # print(len(test_sets[1]))
    #
    # for tr in range(5):
    #     print(test_sets[1][tr])


    # for t, te in KFold(n_splits=2).split(data_set):
    #     print(t)
    #     print(te)
    #     print()

    #
    mal_url = MaliciousUrl()

    neural_net = NeuralNetwork(train_set)
    dummy_model = DummySystem(train_set)
    svm_module = SVMModule(train_set)
    decision_tree_module = DecisionTreeModule(train_set)
    perceptron_model = PerceptronModule(train_set)
    naive_bayes_module = NaiveBayesModule(train_set)
    knn_module = KNNModule(train_set)
    randomforestmodule = RandomForestModule(train_set)

    # models are trained in add_model function
    # (for better time measure control)

    mal_url.add_model(neural_net)
    mal_url.add_model(svm_module)
    mal_url.add_model(perceptron_model)
    mal_url.add_model(decision_tree_module)
    mal_url.add_model(naive_bayes_module)
    mal_url.add_model(knn_module)
    mal_url.add_model(randomforestmodule)
    mal_url.add_model(dummy_model, .1)
    #

    mal_url.train_all_models()
    mal_url.check_statistics(test_set)
    mal_url.view_statistics()
    # # mal_url.get_charts(view_on_screen=1)
    #
    mal_url.reset_statitisc()
    mal_url.check_statistics_with_cross_validation(train_sets, test_sets)
    mal_url.view_statistics()

    result = mal_url.check_final_statistics(test_set, 1)
    #
    mal_url.calculate_weight_of_algorithm()
    mal_url.check_final_statistics(test_set, 1, 1)
    #
    # mal_url.statistics_to_csv('total_statistic')
    mal_url.statistics_to_excel('total_result')


#
# dict_of_weight={'precision': 10000, 'recall': 10000, 'true_positives': 1,
#                                                           'false_positives': 1, 'false_negatives': 1,
#                                                           'true_negatives': 1, 'test_time': 100, 'train_time': 100}